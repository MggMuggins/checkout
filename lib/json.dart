import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:decimal/decimal.dart';
import 'package:path/path.dart' as Path;
import 'package:path_provider/path_provider.dart';

import 'barcode.dart';

const String INTERNAL_DATA_FILE_NAME = "item_tree.json";
const String INIT_JSON = '{"categories": [{"name": "No Json!","items": [{"name": "No Json!","price": "1.00"}]}]}';

/// Loads the saved json file from the disk, optionally takes a new file path
/// to overwrite the internal file and returns that Json config
Future loadJson({String new_json_path}) async {
    Directory docs_dir = await getApplicationDocumentsDirectory();
    String internal_data_file_path = Path.join(docs_dir.path, INTERNAL_DATA_FILE_NAME);
    
    var internal_file = File(internal_data_file_path);
    
    if (!internal_file.existsSync()) {
        internal_file.createSync();
        internal_file.writeAsStringSync(INIT_JSON);
    }
    
    //String data;
    if (new_json_path != null) {
        print(new_json_path);
        final new_json_file = File(new_json_path);
        var bytes = new_json_file.readAsBytesSync();
        internal_file.writeAsBytesSync(bytes); // implicit overwrite
    }
    String data = internal_file.readAsStringSync();
    print(data);
    
    Map items = json.decode(data);
    return JsonConfig.fromJson(items);
}

// Maps to a button_tree.json
class JsonConfig {
    final List<ItemCategory> categories;
    
    JsonConfig.fromJson(Map<String, dynamic> json)
        : categories = List<ItemCategory>.from(json["categories"].map((category) {
                return ItemCategory.fromJson(category);
            }).toList());
    
    Iterable<Item> items() {
        return this.categories.expand((category) => category.items );
    }
    
    @override
    String toString() {
        String categories = this.categories.toString();
        return 'JsonConfig { categories: $categories }';
    }
}

class ItemCategory {
    final String name;
    final List<Item> items;
    
    ItemCategory.fromJson(Map<String, dynamic> json)
        : name = json["name"],
          items = List<Item>.from(json["items"].map((item) {
              return Item.fromJson(item);
          }).toList());
    
    @override
    String toString() {
        return 'ItemCategory { name: "$name", items: $items }';
    }
}

class Item {
    final String name;
    final Decimal price;
    
    Item(this.name, this.price);
    
    Item.fromJson(Map<String, dynamic> json)
        : name = json["name"],
          price = Decimal.parse(json["price"]);
    
    @override
    String toString() {
        return 'Item { name: "$name", price: "$price" }';
    }
}
