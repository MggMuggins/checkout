import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_document_picker/flutter_document_picker.dart';

import 'package:decimal/decimal.dart';
import 'package:event_bus/event_bus.dart';
import 'package:qr_mobile_vision/qr_camera.dart';
import 'package:qr_mobile_vision/qr_mobile_vision.dart';

import 'barcode.dart';
import 'cart.dart';
import 'json.dart';

void main() => runApp(CheckoutApp());

class CheckoutApp extends StatefulWidget {
    CheckoutApp({Key key}) : super(key: key);
    
    @override
    CheckoutAppState createState() => CheckoutAppState();
}

class CheckoutAppState extends State<CheckoutApp> {
    JsonConfig json;
    EventBus bus;
    bool isReadingBarcode;
    
    CheckoutAppState()
        : json = JsonConfig.fromJson(jsonDecode(INIT_JSON)),
          bus = EventBus(),
          isReadingBarcode = false;
    
    @override
    void initState() {
        loadJson().then((json) {
            setState(() {
                this.json = json;
            });
        });
    }
    
    /// Use the document picker to ask the user for a file and set this.json
    /// to the parsed contents of that file
    void pickAndSetJson() {
        var params = FlutterDocumentPickerParams(allowedFileExtensions: ["json"]);
        FlutterDocumentPicker.openDocument(params: params).then((path) {
            loadJson(new_json_path: path).then((config) {
                this.json = config;
            });
        });
    }
    
    void showItemNotRecognized(BuildContext context) {
        showDialog(
            context: context,
            builder: (BuildContext context) {
                return AlertDialog(
                    title: Text("Item not Found"),
                    actions: <Widget>[
                        FlatButton(
                            child: Text("Ok"),
                            onPressed: () => Navigator.pop(context),
                        ),
                    ],
                );
            }
        );
    }
    
    @override
    Widget build(BuildContext context) {
        String app_name = "Checkout";
        
        ThemeData app_theme = ThemeData.light().copyWith(
            primaryColor: Colors.green,
        );
        print("Configuration: ${this.json}");
        
        return MaterialApp(
            title: app_name,
            theme: app_theme,
            home: Scaffold(
                appBar: AppBar(
                    title: Text(app_name),
                    actions: <Widget>[
                        IconButton(
                            icon: Icon(Icons.insert_drive_file),
                            onPressed: this.pickAndSetJson,
                        ),
                        /* For debugging
                        IconButton(
                            icon: Icon(Icons.add_box),
                            onPressed: () {
                                this.bus.fire(CartAddItemEvent(
                                    Item(
                                        "Test Item",
                                        Decimal.fromInt(1),
                                    ),
                                ));
                            }
                        ),
                        IconButton(
                            icon: Icon(Icons.add_circle),
                            onPressed: () {
                                this.bus.fire(CartAddItemEvent(
                                    Item(
                                        "Other Test Item",
                                        Decimal.fromInt(2),
                                    ),
                                ));
                            }
                        ),*/
                    ],
                ),
                body: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                        Expanded(
                            child: QrCamera(
                                formats: <BarcodeFormats>[
                                    BarcodeFormats.DATA_MATRIX,
                                ],
                                qrCodeCallback: (String codeData) {
                                    if (this.isReadingBarcode) {
                                        setState(() {
                                            this.isReadingBarcode = false;
                                        });
                                        Item match = this.json?.items()
                                            // The barcode data is already a hash
                                            .singleWhere(
                                                (item) => codeData == getBarcodeHash(item.name),
                                                orElse: () {
                                                    print("Item not recognized");
                                                    this.showItemNotRecognized(context);
                                                }
                                            );
                                        this.bus.fire(CartAddItemEvent(match));
                                    }
                                },
                                child: Container(
                                    alignment: Alignment.centerLeft,
                                    margin: EdgeInsets.all(8.0),
                                    child: FloatingActionButton(
                                        child: Icon(
                                            Icons.camera_alt,
                                            size: 48.0,
                                            color: Colors.black,
                                        ),
                                        backgroundColor: Colors.white,
                                        onPressed: () {
                                            setState(() {
                                                this.isReadingBarcode = true;
                                            });
                                        }
                                    ),
                                ),
                            ),
                        ),
                        Expanded(
                            child: Cart(this.bus)
                        ),
                    ],
                ),
            )
        );
    }
}
