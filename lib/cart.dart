import 'package:flutter/material.dart';

import 'package:decimal/decimal.dart';
import 'package:event_bus/event_bus.dart';

import 'json.dart';

class CartAddItemEvent {
    Item item;
    
    CartAddItemEvent(this.item);
}

class CartRemoveItemEvent {
    int index;
    
    CartRemoveItemEvent(this.index);
}

class CartItem {
    final Item item;
    int quantity;
    
    CartItem(this.item, this.quantity);
    
    Decimal price() {
        return this.item.price * Decimal.fromInt(this.quantity);
    }
    
    Widget build(BuildContext context, EventBus bus, int index) {
        return Container(
            margin: EdgeInsets.all(2.0),
            child: Row(
                children: <Widget>[
                    Expanded(
                        child: Text(
                            this.item.name,
                            style: Theme.of(context).textTheme.display1,
                        ),
                    ),
                    Text(
                        "\$${this.item.price.toStringAsFixed(2)}",
                        style: Theme.of(context).textTheme.display1,
                    ),
                    IconButton(
                        icon: Icon(Icons.remove),
                        onPressed: () {
                            print("remove pressed: $index");
                            bus.fire(CartRemoveItemEvent(index));
                        }
                    ),
                ],
            ),
        );
    }
}

class Cart extends StatefulWidget {
    EventBus bus;
    
    Cart(EventBus eventBus, {Key key})
        : bus = eventBus,
          super(key: key);
    
    CartState createState() => CartState(this.bus);
}

class CartState extends State<Cart> {
    /// Use a map for faster lookup and merging
    List<CartItem> cartContents;
    EventBus bus;
    
    CartState(this.bus) {
        this.cartContents = List();
        
        bus.on<CartAddItemEvent>().listen((event) {
            setState(() {
                final item = CartItem(event.item, 1);
                this.cartContents.add(item);
            });
        });
        
        bus.on<CartRemoveItemEvent>().listen((event) {
            setState(() {
                this.cartContents.removeAt(event.index);
            });
        });
    }
    
    Decimal price() {
        return this.cartContents
            .fold(Decimal.fromInt(0), (Decimal agregate, CartItem value) => agregate += value.price());
    }
    
    /// Calling this function actually clears the cart. Careful.
    void checkOut() {
        //TODO: Db/data recording here
        this.cartContents.clear();
    }
    
    void checkOutCash(BuildContext context) {
        showDialog(
            context: context,
            builder: (BuildContext context) {
                return AlertDialog(
                    title: this.totalRow(),
                    actions: <Widget>[
                        FlatButton(
                            child: Text(
                                "CANCEL",
                                style: Theme.of(context).textTheme.button,
                            ),
                            onPressed: () => Navigator.pop(context),
                        ),
                        RaisedButton(
                            child: Text(
                                "OK",
                                style: Theme.of(context).textTheme.button,
                            ),
                            onPressed: () {
                                this.checkOut();
                                Navigator.pop(context);
                            },
                        ),
                    ],
                );
            },
        );
    }
    
    void checkOutPaypal() {
        
    }
    
    /// Returns a row widget that shows the total (and label)
    ///   on opposite ends
    Widget totalRow() {
        var price_string = "\$" + (this.price().toStringAsFixed(2));
        return Row(
            children: <Widget> [
                Expanded(
                    child: Container(
                        child: Text(
                            "Total:",
                            style: Theme.of(context).textTheme.display1,
                        ),
                    ),
                ),
                Expanded(
                    child: Container(
                        child: Text(
                            price_string,
                            textAlign: TextAlign.right,
                            style: Theme.of(context).accentTextTheme.display1,
                        ),
                    ),
                ),
            ],
        );
    }
    
    Widget cartListLabel() {
        if (this.cartContents.isEmpty) {
            return Text(
                "Cart Empty",
                style: Theme.of(context).textTheme.display1,
            );
        } else {
            return null;
        }
    }
    
    @override
    Widget build(BuildContext context) {
        var theme = Theme.of(context);
        return Column(
            children: <Widget>[
                Expanded(
                    child: Stack(
                        children: <Widget>[
                            Center(
                                child: this.cartListLabel(),
                            ),
                            ListView.builder(
                                itemCount: this.cartContents.length,
                                //shrinkWrap: true,
                                itemBuilder: (context, index) {
                                    return this.cartContents[index].build(context, this.bus, index);
                                },
                            ),
                        ],
                    ),
                ),
                Container(
                    padding: EdgeInsets.all(2.0),
                    child: Column(
                        children: <Widget>[
                            this.totalRow(),
                            // Checkout Buttons
                            Row(
                                children: <Widget>[
                                    Expanded(
                                        child: Container(
                                            padding: EdgeInsets.only(
                                                right: 2.0,
                                            ),
                                            child: RaisedButton(
                                                child: Text(
                                                    "CASH",
                                                    style: theme.textTheme.button,
                                                ),
                                                onPressed: () {
                                                    if (!this.cartContents.isEmpty) {
                                                        this.checkOutCash(context);
                                                    }
                                                }
                                            ),
                                        ),
                                    ),
                                    Expanded(
                                        child: Container(
                                            padding: EdgeInsets.only(
                                                left: 2.0,
                                            ),
                                            child: RaisedButton(
                                                child: Text(
                                                    "PAYPAL",
                                                    style: theme.textTheme.button,
                                                ),
                                                onPressed: this.checkOutPaypal
                                            ),
                                        ),
                                    ),
                                ],
                            ),
                        ],
                    ),
                ),
            ],
        );
    }
}
