import 'dart:convert';
import 'dart:typed_data';

import 'package:crypto/crypto.dart';

// Only cruddy languages don't implement this
Iterable<int> _range(int low, int high) sync* {
  for (int i = low; i < high; ++i) {
    yield i;
  }
}

/// Converts input to a 16 byte list (using md5)
/// Before conversion, the name has two functions called on
/// it: toLowerCase() and replaceAll(' ', '_')
String getBarcodeHash(String input) {
    String simpleString = input.toLowerCase().replaceAll(' ', '_');
    Digest digest = md5.convert(simpleString.codeUnits);
    final bytes = Uint8List.fromList(digest.bytes);
    final result = bytes.map((b) => b.toRadixString(16) );
    return result.join();
    /*List<int> bytes = digest.bytes;
    //var ascii_bytes = List<int>.generate(bytes.length, (i) {
    //    return bytes[i] - 128;
    //});
    print("bytes: $bytes");
    
    var combined = List<int>.generate(4, (int_indx) {
        var four = digest.bytes.take(4);
        print("four: $four");
        int one = 0;
        for (int byte in four) {
            int shift = int_indx * 8;
            one += byte >> shift;
        }
        print("one: $one");
        return one;
    });
    return ascii.decode(bytes, allowInvalid: true);*/
}
