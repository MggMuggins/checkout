# checkout

Full Disclaimer: This project is a re-implementation of a Java/Kotlin android application I wrote a year ago for the same purposes with similar goals. That project is not open-sourced because of legal issues regarding some contributors.

This app implements a Json interface for defining items, an item cart, and a barcode scanner for use as a point-of-sale application. It is cross platform, written using the [Flutter](https://flutter.io/) toolkit in Dart. Extras include interaction with paypal via their sideloader API and privacy-centered data collection.

This is in pretty heavy development for the time being, I'm using it as an excuse to rewrite my old app and for a school project.
